import React, {useState} from 'react';
import Navbar from '../components/Navbar';
import Sidebar from '../components/Sidebar';
import InfoSection from '../components/InfoSection';
import { homeObjOne } from '../components/InfoSection/Data';
import Services from '../components/Services';
import Footer from '../components/Footer';

// import StickyFooter from '../components/StickyFooter/StickyFooter';


export const Home = () => {

    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => {
        setIsOpen(!isOpen)
    };


    return (
      <>

        <Sidebar isOpen={isOpen} toggle={toggle} />
        <Navbar toggle={toggle} /> 

        <InfoSection {...homeObjOne}/> 
        <Services />
        {/* <StickyFooter /> */}
         <Footer /> 

       
        </>   
    );
};

export default Home 