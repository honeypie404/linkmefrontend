import React from 'react';
import {FaFacebook, FaInstagram} from 'react-icons/fa';

import {FooterContainer, FooterWrap, FooterLinksContainer,SocialMediaWrap, 
    SocialLogo,WebsiteRights, SocialIcons, SocialIconLink, 
    SocialMedia} from './FooterElements';

export const Footer = () => {
    return (
        <FooterContainer>
          <FooterWrap>
              <FooterLinksContainer>
                  {/* <FooterLinksWrapper>
                      <FooterLinkItems>
                          <FooterLinkTitle> About Us</FooterLinkTitle>   
                              <FooterLink to="/">How it works</FooterLink>
                              <FooterLink to="/">Testimonials</FooterLink>
                              <FooterLink to="/">Careers</FooterLink>
                              <FooterLink to="/">Terms of Service</FooterLink>
                      </FooterLinkItems>

                      <FooterLinkItems>
                          <FooterLinkTitle> Video</FooterLinkTitle>   
                              <FooterLink to="/">How it works</FooterLink>
                              <FooterLink to="/">Testimonials</FooterLink>
                              <FooterLink to="/">Careers</FooterLink>
                              <FooterLink to="/">Terms of Service</FooterLink>
                      </FooterLinkItems>
                  </FooterLinksWrapper> */}

                  
                  {/* <FooterLinksWrapper> 
                      <FooterLinkItems>
                          <FooterLinkTitle>Contact Us</FooterLinkTitle>   
                              <FooterLink to="/">How it works</FooterLink>
                              <FooterLink to="/">Testimonials</FooterLink>
                              <FooterLink to="/">Careers</FooterLink>
                              <FooterLink to="/">Investors</FooterLink>
                              <FooterLink to="/">Terms of Service</FooterLink>
                      </FooterLinkItems>

                      <FooterLinkItems>
                          <FooterLinkTitle>Social Media</FooterLinkTitle>   
                              <FooterLink to="/">Facebook</FooterLink>
                              <FooterLink to="/">Instagram</FooterLink>
                              <FooterLink to="/">Youtube</FooterLink>
                              <FooterLink to="/">Twitter</FooterLink>
                      </FooterLinkItems>
                  </FooterLinksWrapper> */}
                  
              </FooterLinksContainer>

              <SocialMedia>
                <SocialMediaWrap>
                    <SocialLogo to='/'> Linkme </SocialLogo>
                  <WebsiteRights>
                      Linkme © {new Date().getFullYear()} All rights reserved.
                  </WebsiteRights>
                      <SocialIcons>
                          <SocialIconLink href="/" target="_blank" aria-label="Facebook" > <FaFacebook /> </SocialIconLink>
                          <SocialIconLink href="/" target="_blank" aria-label="Instagram" > <FaInstagram /> </SocialIconLink>
                          <SocialIconLink href="/" target="_blank" aria-label="Facebook" > <FaFacebook /> </SocialIconLink>
                      </SocialIcons>
                 
                </SocialMediaWrap>
              </SocialMedia>

          </FooterWrap>
        </FooterContainer>
    )
}

export default Footer
